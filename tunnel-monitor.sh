#!/bin/bash

REMOTE_USER="User" # The username for the remote SSH server.
REMOTE_HOST="IP/DOMAIN" # The IP address or hostname of the remote SSH server.
REMOTE_PORT="22" # The SSH port on the remote server.
# Define local ports and their corresponding remote ports, ["localport"]="remoteport"
declare -A PORTS_MAPPING=( ["25565"]="25565" ["80"]="8080" ["9443"]="443" )
SLEEP="60"

declare -A PIDS  # Associative array to store PIDs for each port

# Function to check whether remote ports are up or down
function check_tunnel() {
    local result=0

    for local_port in "${!PORTS_MAPPING[@]}"; do
        remote_port=${PORTS_MAPPING[$local_port]}
        if ssh $REMOTE_USER@$REMOTE_HOST -p $REMOTE_PORT "nc -zv localhost $remote_port 2>/dev/null"; then
            echo "Tunnel [$local_port]=$remote_port with PID ${PIDS["$local_port"]} is up"
        else
            echo "Tunnel [$local_port]=$remote_port is down, starting..."
            result=1
        fi
    done

    return $result
}

# Function to stop specific tunnel processes
function stop_tunnel() {
    for local_port in "${!PORTS_MAPPING[@]}"; do
        local pid=${PIDS["$local_port"]}
        if [ -n "$pid" ]; then
            remote_port=${PORTS_MAPPING[$local_port]}
            if ! ssh $REMOTE_USER@$REMOTE_HOST -p $REMOTE_PORT "nc -zv localhost $remote_port 2>/dev/null"; then
                echo "Stopping SSH tunnel [$local_port]=$remote_port with PID $pid"
                kill $pid 2>/dev/null
                wait $pid 2>/dev/null
                unset PIDS["$local_port"]
            fi
        fi
    done
}

# Function to start all tunnel processes
function start_tunnel() {
    for local_port in "${!PORTS_MAPPING[@]}"; do
        local existing_pid=${PIDS["$local_port"]}
        if [ -z "$existing_pid" ] || ! ps -p "$existing_pid" &>/dev/null; then
            remote_port=${PORTS_MAPPING[$local_port]}
            ssh -nNT $REMOTE_USER@$REMOTE_HOST -p $REMOTE_PORT -R $remote_port:localhost:$local_port &
            PIDS["$local_port"]=$!
            echo "SSH tunnel [$local_port]=$remote_port started with PID ${PIDS["$local_port"]}"
        fi
    done

    sleep 5  # Add a delay to allow time for the tunnels to start
}

# Function to handle cleanup on script exit or interrupt signal
function cleanup() {
    echo "Cleaning up..."

    # Kill all SSH tunnel processes
    for pid in "${PIDS[@]}"; do
        [ -n "$pid" ] && kill $pid
    done

    exit 0
}

# Trap the script exit and interrupt signal, and call the cleanup function
trap cleanup EXIT INT

while true; do
    if check_tunnel; then
        for local_port in "${!PORTS_MAPPING[@]}"; do
            remote_port=${PORTS_MAPPING[$local_port]}
        done
    else
        stop_tunnel
        start_tunnel
    fi

    sleep $SLEEP
done