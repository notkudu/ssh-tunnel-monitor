# SSH Tunneling Script

## Overview

This script sets up and monitors SSH tunnels for reverse port forwarding. It is designed to be used with multiple ports, checking their status and restarting the tunnels if necessary.

## NOTE

it's recommended to use an SSH key without a password, especially when setting up the script for auto-start with a systemd service. This ensures that the script can run without manual intervention during system boot.

## Features

- **Dynamic Tunnel Management:** Monitors the status of specified ports and restarts tunnels as needed.
- **Cleanup on Exit:** Automatically stops all active tunnel processes on script exit.

## Usage

1. **Clone the Repository:**
    ```bash
    git clone https://gitlab.com/notkudu/ssh-tunnel-monitor.git
    cd ssh-tunnel-monitor
    ```

2. **Make the Script Executable:**
    ```bash
    chmod +x tunnel-monitor.sh
    ```

3. **Modify Configuration:**
    Edit the script to set your remote SSH server details and define the ports you want to tunnel.

4. **Run the Script:**
    ```bash
    ./tunnel-monitor.sh
    ```

5. **Exiting the Script:**
    Press `Ctrl+C` to gracefully stop the script and terminate all active tunnel processes.

## Configuration

Modify the script variables to match your setup:

- `REMOTE_USER`: The username for the remote SSH server.
- `REMOTE_HOST`: The IP address or hostname of the remote SSH server.
- `REMOTE_PORT`: The SSH port on the remote server.
- `LOCAL_PORTS`: An array of local ports to remote ports to be tunneled.
- `SLEEP`: Amount of time the script takes to recheck in seconds.

## Auto-Start with Systemd

To ensure your SSH tunneling script starts automatically on system boot, you can create a systemd service:

1. **Create a systemd Service File:**
    Create a new file `/etc/systemd/system/tunnel-monitor.service` with the following content:

    ```ini
    [Unit]
    Description=SSH Tunneling Monitor
    After=network.target

    [Service]
    Type=simple
    ExecStart=/path/to/your/tunnel-monitor.sh
    Restart=always
    User=your-username

    [Install]
    WantedBy=default.target
    ```

    Replace `/path/to/your/tunnel-monitor.sh` with the actual path to your tunnel script. Adjust `User` to match the user you want the script to run as.

2. **Reload systemd:**
    ```bash
    sudo systemctl daemon-reload
    ```

3. **Enable and Start the Service:**
    ```bash
    sudo systemctl enable tunnel-monitor
    sudo systemctl start tunnel-monitor
    ```

4. **Check Service Status:**
    ```bash
    sudo systemctl status tunnel-monitor
    ```

Now, your SSH tunneling Monitor script will automatically start on system boot and restart if it crashes.
